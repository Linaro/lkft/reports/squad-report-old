import re
import os
import sys
import json
import email
from json import JSONDecodeError
from urllib.parse import urljoin

import jinja2
from squad_client.core.models import Project

import report_config as config
from squad_client.core.api import SquadApi
from utils.urls import suite_name_from_url, environment_name_from_url
from utils.parsing import (
    parse_job_definitions,
    get_triplet_from_setup,
    matches_one_of,
    build_patterns,
)
from utils.jobs import job_to_json
from utils.env_vars import (
    ENV_JOBS,
    ENV_JOB_DETAILS_URL,
    DIRECTORY_REPORTS,
    DIRECTORY_JOBS,
    DIRECTORY_SUMMARIES,
)
from utils.env_vars import REGEXP_STABLE_BUILD


def complement_triplets(squad, job, include_unfinished=False):
    """Make sure the triplets are fully qualified (all 3 values are real)"""
    errors = []

    build_triplet = job.get("build")
    previous_triplet = job.get("previous")
    base_triplet = job.get("base")

    build_iters = {}

    group = build_triplet.get("group")
    project = build_triplet.get("project")
    build = build_triplet.get("build")
    if not build or build == "?":
        key = "{0}:{1}".format(group.slug, project.slug)
        build_iter = build_iters.get(key)
        if not build_iter:
            project.build_collection = BuildCollection(project, include_unfinished)
            build_iter = build_iters[key] = project.build_collection.__iter__()
        build = build_iter.next()
        if not build:
            errors.append("Project {0} has no builds".format(project.slug))
            return errors

        build_triplet["build"] = build

    if previous_triplet:
        group = previous_triplet.get("group")
        project = previous_triplet.get("project")
        build = previous_triplet.get("build")
        if not build or build == "?":
            key = "{0}:{1}".format(group.slug, project.slug)
            build_iter = build_iters.get(key)
            if not build_iter:
                project.build_collection = BuildCollection(project, include_unfinished)
                build_iter = build_iters[key] = project.build_collection.__iter__()
            build = build_iter.peek()
            if not build:
                del job["previous"]
            else:
                previous_triplet["build"] = build

    if base_triplet:
        group = base_triplet.get("group")
        project = base_triplet.get("project")
        build = base_triplet.get("build")
        if not build or build == "?":
            key = "{0}:{1}".format(group.slug, project.slug)
            build_iter = build_iters.get(key)
            if not build_iter:
                project.build_collection = BuildCollection(project, include_unfinished)
                build_iter = build_iters[key] = project.build_collection.__iter__()

            last_parts = None
            for build in build_iter:
                parts = get_build_version_parts(build.version)
                if not parts:
                    continue
                if not parts.get("count"):
                    base_triplet["build"] = build
                    break
                if last_parts and (
                    parts.get("major") != last_parts.get("major")
                    or parts.get("minor") != last_parts.get("minor")
                    or parts.get("patch") != last_parts.get("patch")
                ):
                    base_triplet["build"] = build
                    break
                last_parts = parts

            if not build or build == "?":
                del job["base"]

    return errors


def increment_build_counter(stats, counter_type):
    if stats["build"].get(counter_type) == None:
        stats["build"][counter_type] = 0
    stats["build"][counter_type] += 1


def increment_environment_counter(stats, environment, counter_type):
    env_record = stats["environments"].get(environment)
    if not env_record:
        env_record = stats["environments"][environment] = {
            "suites": {},
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
        }
    if env_record.get(counter_type) == None:
        env_record[counter_type] = 0
    env_record[counter_type] += 1


def increment_suite_counter(stats, environment, suite, counter_type, metrics=None):
    metrics = [] if metrics is None else metrics
    # suite counter
    suite_record = stats["suites"].get(suite)
    if not suite_record:
        suite_record = stats["suites"][suite] = {
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
            "metrics": {},
        }
    if suite_record.get(counter_type) == None:
        suite_record[counter_type] = 0
    suite_record[counter_type] += 1

    # for each metric add a new key to suite_record and append testrun data.
    for metric in metrics:
        if metric.short_name not in suite_record["metrics"].keys():
            suite_record["metrics"][metric.short_name] = []
        suite_record["metrics"][metric.short_name].append(metric.result)

    # suite-in-environment counter
    env_record = stats["environments"].get(environment)
    if not env_record:
        env_record = stats["environments"][environment] = {
            "suites": {},
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
        }
    suite_record = env_record["suites"].get(suite)
    if not suite_record:
        suite_record = env_record["suites"][suite] = {
            "pass": 0,
            "fail": 0,
            "skip": 0,
            "xfail": 0,
        }
    if suite_record.get(counter_type) == None:
        suite_record[counter_type] = 0


def get_build_stats(squad, build_triplet, environments, suites, performance):
    group = build_triplet["group"]
    project = build_triplet["project"]
    build = build_triplet["build"]

    build_stats = {
        "build": {"pass": 0, "fail": 0, "skip": 0, "xfail": 0},
        "environments": {},
        "suites": {},
        "failures": [],
        "skips": [],
    }

    # fetch all metrics for this build and organize them by testrun and suite
    metrics_for_build = {}
    metrics_per_testrun_and_suite = {}
    if performance is True:
        metrics_for_build = squad.metrics(test_run__build=build.id, count=-1)
    for m in metrics_for_build.values():
        if m.test_run not in metrics_per_testrun_and_suite:
            metrics_per_testrun_and_suite[m.test_run] = {}
        if m.suite not in metrics_per_testrun_and_suite[m.test_run]:
            metrics_per_testrun_and_suite[m.test_run][m.suite] = []
        metrics_per_testrun_and_suite[m.test_run][m.suite].append(m)

    for test_id, test in build.tests(
        fields="environment,status,suite,name,test_run"
    ).items():
        increment_build_counter(build_stats, test.status)

        env_name = environment_name_from_url(squad, test.environment)
        if environments and not matches_one_of(env_name, environments):
            continue

        suite_name = suite_name_from_url(project, test.suite)
        if suites and not matches_one_of(suite_name, suites):
            continue

        # filter metrics by matching current testrun and suite
        m = None
        if (
            test.test_run in metrics_per_testrun_and_suite
            and test.suite in metrics_per_testrun_and_suite[test.test_run]
        ):
            m = metrics_per_testrun_and_suite[test.test_run].pop(test.suite)

        increment_environment_counter(build_stats, env_name, test.status)
        increment_suite_counter(build_stats, env_name, test.suite, test.status, m)

        if test.status == "fail":
            build_stats["failures"].append((env_name, suite_name, test.name))
            build_stats["build"][test.status] += 1

        elif test.status == "skip":
            build_stats["skips"].append((env_name, suite_name, test.name))
            build_stats["build"][test.status] += 1

        elif test.status == "pass":
            build_stats["build"][test.status] += 1

        elif test.status == "xfail":
            build_stats["build"][test.status] += 1

    sorted_suites = []
    for suite_url, stats in build_stats["suites"].items():
        suite = suite_name_from_url(project, suite_url)
        sorted_suites.append((suite, stats))

    print("sorted_suites:", sorted_suites)

    sorted_suites = sorted(
        ((suite, stats) for suite, stats in sorted_suites), key=lambda item: item[0]
    )

    build_stats["suites"] = sorted_suites

    # sort all suites under environments
    for environment, stats in build_stats["environments"].items():
        suites = stats["suites"]
        sorted_suites = sorted(
            (
                (
                    suite_name_from_url(project, url),
                    stats,
                )
                for url, stats in suites.items()
            ),
            key=lambda item: item[0],
        )
        stats["suites"] = sorted_suites

    sorted_environments = sorted(
        ((name, stats) for name, stats in build_stats["environments"].items()),
        key=lambda item: item[0],
    )
    build_stats["environments"] = sorted_environments

    return build_stats


def get_build_version_parts(build_version):
    match = re.match(REGEXP_STABLE_BUILD, build_version)
    if not match:
        return None

    result = {}

    value = match.group("major")
    if value:
        result["major"] = value

    value = match.group("minor")
    if value:
        result["minor"] = value

    value = match.group("patch")
    if value:
        result["patch"] = value

    value = match.group("count")
    if value:
        result["count"] = value

    value = match.group("hash")
    if value:
        result["hash"] = value

    return result


def list_builds(squad, args):
    """
    List all the builds for each of the projects that match the regexp.
    """

    success, jobs = parse_job_definitions(squad, args, True)
    if not success:
        return 1

    if not jobs:
        groups = squad.groups(count=-1)
        if not groups:
            print("There are no groups")
        else:
            print("Groups:")
            for group in groups.values():
                print(
                    "- {0}{1}".format(
                        group.slug, (" ({0})".format(group.name) if group.name else "")
                    )
                )
        return 0

    for job_id, job in enumerate(jobs):
        print("Job {0}:".format(job_id))
        build_triplet = job.get("build")
        previous_triplet = job.get("previous")
        base_triplet = job.get("base")

        num_triplets = (
            (1 if build_triplet else 0)
            + (1 if previous_triplet else 0)
            + (1 if base_triplet else 0)
        )

        if num_triplets > 1:  # full job specification
            complement_triplets(squad, job, args.all)

            for triplet_type in ("build", "previous", "base"):
                triplet = job.get(triplet_type)
                if not triplet:
                    continue
                group = triplet.get("group")
                project = triplet.get("project")
                build = triplet.get("build")
                print(
                    "- {0}: {1}:{2}{3}".format(
                        triplet_type,
                        group.slug
                        if group and group != "?"
                        else ("?" if group else ""),
                        project.slug
                        if project and project != "?"
                        else ("?" if group else ""),
                        (":" + build.version)
                        if build and build != "?"
                        else (":?" if build else ""),
                    )
                )

        else:
            for triplet_type in ("build", "previous", "base"):
                triplet = job.get(triplet_type)
                if not triplet:
                    continue
                group = triplet.get("group")
                project = triplet.get("project")
                build = triplet.get("build")
                print(
                    "- {0}: {1}:{2}{3}".format(
                        triplet_type,
                        group.slug
                        if group and group != "?"
                        else ("?" if group else ""),
                        project.slug
                        if project and project != "?"
                        else ("?" if group else ""),
                        (":" + build.version)
                        if build and build != "?"
                        else (":?" if build else ""),
                    )
                )
                if project == "?":
                    projects = group.projects(count=-1)
                    for project in projects.values():
                        print("    {0}:{1}".format(group.slug, project.slug))
                elif build == "?":
                    project.build_collection = BuildCollection(project, args.all)
                    if not len(project.build_collection):
                        print("    none")
                    else:
                        for build in project.build_collection:
                            print(
                                "    {0}:{1}:{2}".format(
                                    group.slug, project.slug, build.version
                                )
                            )
                else:
                    print("    {0}:{1}".format(group.slug, project.slug))

    return 0


def scan_builds(squad, args):
    """
    Prepare jobs for the report generation.
    """

    try:
        os.mkdir(DIRECTORY_JOBS)
    except FileExistsError:
        pass
    except:
        print(
            "Exception creating the job directory '{0}': {1}".format(
                DIRECTORY_JOBS, sys.exc_info()[0]
            )
        )
        return 2

    try:
        os.mkdir(DIRECTORY_SUMMARIES)
    except FileExistsError:
        pass
    except:
        print(
            "Exception creating the summaries directory '{0}': {1}".format(
                DIRECTORY_SUMMARIES, sys.exc_info()[0]
            )
        )
        return 2

    try:
        os.mkdir(DIRECTORY_REPORTS)
    except FileExistsError:
        pass
    except:
        print(
            "Exception creating the report directory '{0}': {1}".format(
                DIRECTORY_REPORTS, sys.exc_info()[0]
            )
        )
        return 2

    success, jobs = parse_job_definitions(squad, args)
    if not jobs:
        print("No jobs specified either as args or in {0}. Aborting.".format(ENV_JOBS))
        return 3

    print("Preparing report jobs")

    job_file_counter = 1

    for job_id, job in enumerate(jobs):
        complement_triplets(squad, job, args.all)

        json_job = job_to_json(job)
        print("Job {0}:".format(job_id))
        print(json.dumps(json_job, indent=2))

        job_file_name = "{0}/{1:02d}.{2}.txt".format(
            DIRECTORY_JOBS, job_file_counter, job["build"]["project"].slug
        )
        job_file_counter += 1
        with open(job_file_name, "w") as fp:
            json.dump(json_job, fp, indent=2)

    return 0


def compare_builds(from_build, to_build, args, suites=None, environments=None):
    if suites == None:
        suites = ()

    if environments == None:
        environments = ()

    comparison = Project.compare_builds(from_build.id, to_build.id, force=args.all)

    if comparison["fixes"] and (suites or environments):
        filtered_fixes = {}
        for env in comparison["fixes"]:
            if environments and env not in environments:
                continue
            env_suites = comparison["fixes"][env]
            for suite in env_suites:
                if suites and suite not in suites:
                    continue
                if env not in filtered_fixes:
                    filtered_fixes[env] = {}
                filtered_fixes[env][suite] = env_suites[suite]
        comparison["fixes"] = filtered_fixes

    if comparison["regressions"] and (suites or environments):
        filtered_regressions = {}
        for env in comparison["regressions"]:
            if environments and env not in environments:
                continue
            env_suites = comparison["regressions"][env]
            for suite in env_suites:
                if suites and suite not in suites:
                    continue
                if env not in filtered_regressions:
                    filtered_regressions[env] = {}
                filtered_regressions[env][suite] = env_suites[suite]
        comparison["regressions"] = filtered_regressions

    return comparison


def build_report(squad, args, report_setup):
    environments = build_patterns(args.environments)
    suites = build_patterns(args.suites)

    if (args.environments and not environments) or (args.suites and not suites):
        print("Aborted")
        return 1

    build = report_setup.get("build")
    previous = report_setup.get("previous")
    base = report_setup.get("base")

    build_triplet = get_triplet_from_setup(squad, build) if build else None
    previous_triplet = get_triplet_from_setup(squad, previous) if previous else None
    base_triplet = get_triplet_from_setup(squad, base) if base else None

    print(
        "Generating report for build {0} of {1} ...".format(
            build_triplet["build"].version, build_triplet["project"].slug
        )
    )

    if environments:
        print(
            "Environments to report for: {0}".format(
                ", ".join([name for name, _ in environments])
            )
        )

    if suites:
        print(
            "Suites to report for: {0}".format(", ".join([name for name, _ in suites]))
        )

    if previous:
        compared_to_previous = compare_builds(
            previous_triplet["build"],
            build_triplet["build"],
            args,
            suites,
            environments,
        )
    else:
        compared_to_previous = None

    if base_triplet and base_triplet["build"] != build_triplet["build"]:
        compared_to_base = compare_builds(
            base_triplet["build"], build_triplet["build"], args, suites, environments
        )

    else:
        base_triplet = None
        compared_to_base = None

    build_details_url = urljoin(
        SquadApi.url,
        "%s/%s/build/%s"
        % (
            build_triplet["group"].slug,
            build_triplet["project"].slug,
            build_triplet["build"].version,
        ),
    )

    template_env = jinja2.Environment(
        extensions=["jinja2.ext.loopcontrols"],
        loader=jinja2.FileSystemLoader("templates"),
        trim_blocks=True,
        lstrip_blocks=True,
    )

    build_stats = get_build_stats(
        squad, build_triplet, environments, suites, args.performance
    )

    report = template_env.get_template("stable-build-report.jinja").render(
        build=build_triplet,
        previous=previous_triplet,
        base=base_triplet,
        compared_to_previous=compared_to_previous,
        compared_to_base=compared_to_base,
        build_details_url=build_details_url,
        build_stats=build_stats,
        environments=environments,
        suites=suites,
    )

    msg = email.message_from_bytes(bytearray(report, "utf-8"))

    # remove and readd after other headers to get it as a last header
    subject = msg["Subject"]
    del msg["Subject"]

    setup = {"From": "LKFT <lkft@linaro.org>", "To": "LKFT <lkft@linaro.org>"}
    for regexp, a_setup in config.report_email_by_project:
        match = re.match(regexp, build_triplet["project"].slug)
        if match:
            setup.update(a_setup)
            break

    msg["From"] = setup.get("From", config.default_report_from)

    values = setup.get("To", config.default_report_to)
    msg["To"] = ", ".join(values)

    values = setup.get("Cc", config.default_report_cc)
    if values and len(values):
        msg["Cc"] = ", ".join(values)

    msg["Subject"] = subject

    file_name = "{0}/{1}--{2}--{3}.eml.txt".format(
        DIRECTORY_REPORTS,
        build_triplet["group"].slug,
        build_triplet["project"].slug,
        build_triplet["build"].version,
    )

    with open(
        file_name,
        "w",
    ) as f:
        f.write(msg.as_string(policy=msg.policy.clone(max_line_length=0)))
    print("... wrote the report into {0}".format(file_name))

    summary = template_env.get_template("stable-build-summary.jinja").render(
        build=build_triplet,
        previous_build=previous_triplet,
        base_build=base_triplet,
        compared_to_previous=compared_to_previous,
        compared_to_base=compared_to_base,
        build_details_url=build_details_url,
        build_stats=build_stats,
        details_url=os.environ.get(ENV_JOB_DETAILS_URL),
    )
    summary_file_name = "{0}/{1}.txt".format(
        DIRECTORY_SUMMARIES, build_triplet["project"].slug
    )
    print("Summary in {0}:\n{1}\n".format(summary_file_name, summary))
    with open(summary_file_name, "a") as fp:
        fp.write(summary)

    return 0


def process_job_file(squad, args, job_file_name, job_index=None):
    """
    Generate reports for a single job file (i.e. single project)
    """

    try:
        with open(job_file_name, "r") as fd:
            json_text = fd.read()
    except OSError as e:
        print(
            "{0}Unable to read job file '{1}': {2}".format(
                "({0}) ".format(job_index) if job_index != None else "",
                job_file_name,
                e.strerror,
            )
        )
        return

    try:
        setup = json.loads(json_text)
    except JSONDecodeError as e:
        print(
            "{0}Unable to parse job file '{1}': {2}".format(
                "({0}) ".format(job_index) if job_index != None else "",
                job_file_name,
                e.msg,
            )
        )
        return

    print(
        "{0}Processing job {1}:\n{2}".format(
            "({0}) ".format(job_index) if job_index != None else "",
            job_file_name,
            json.dumps(setup, indent=2),
        )
    )

    build_report(squad, args, setup)


class BuildCollection(object):
    """
    Build collection that allows working with a set of builds.
    """

    class Iterator(object):
        """
        Build collection iterator, that allows peeking at non-current items.
        """

        def __init__(self, collection, offset=None):
            self.collection = collection
            self.pointer = (
                (offset if offset != None else 0) if len(collection.builds) else None
            )

        def __iter__(self):
            return self

        def __next__(self):
            item = self.next()
            if item == None:
                raise StopIteration()

            return item

        def __len__(self):
            if self.pointer == None:
                return 0

            return len(self.collection.builds) - self.pointer

        def clone(self):
            return BuildCollection.Iterator(self.collection, self.pointer)

        def next(self):
            if self.pointer == None:
                return None

            item = self.collection.builds[self.pointer]
            self.pointer += 1
            if self.pointer >= len(self.collection.builds):
                self.pointer = None
            return item

        def peek(self, offset=0):
            if self.pointer == None:
                return None

            peek_at = self.pointer + offset
            if peek_at < 0 or peek_at >= len(self.collection.builds):
                return None

            return self.collection.builds[peek_at]

        def reset(self):
            self.pointer = None if not len(self.collection.builds) else 0

        def collection(self):
            return self.collection

    # end of class Iterator

    def __init__(self, project, include_unfinished=False):
        self.project = project
        if include_unfinished:
            builds = project.builds(count=-1)
        else:
            builds = project.builds(count=-1, status__finished=True)
        self.by_version = {}
        self.builds = []
        if builds:
            for build_id, build in builds.items():
                self.by_version[build.version] = build
                self.builds.append(build)

    def __iter__(self):
        return self.Iterator(self)

    def values(self):
        return self.Iterator(self)

    def build(self, version_name):
        return self.by_version.get(version_name)

    def __len__(self):
        return len(self.builds)


# end of class BuildCollection
