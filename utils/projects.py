g_group_project_index = {}


def get_project(squad, group, project_slug):
    global g_group_project_index

    project_index = g_group_project_index.get(group.slug)
    if not project_index:
        group_projects = group.projects(count=-1)
        project_index = {project.slug: project for project in group_projects.values()}
        g_group_project_index[group.slug] = project_index

    return project_index.get(project_slug)
