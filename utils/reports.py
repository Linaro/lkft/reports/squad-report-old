import re
import os
import sys
import email
from email import policy
from email.message import EmailMessage

import report_config as config

from utils.env_vars import (
    REGEXP_STABLE_BUILD,
    ENV_AWS_EMAIL_REGION,
    REGEXP_REPORT_FILE_NAME,
)
from utils.builds import process_job_file
from utils.env_vars import ENV_AWS_MODE, DIRECTORY_REPORTS, DIRECTORY_JOBS
from utils.env_vars import ENV_PARALLEL_JOB_ID, ENV_PARALLEL_JOB_TOTAL
from utils.env_vars import DIRECTORY_SUMMARIES

# url length limit
MAX_URL_LENGTH = 2000

# deployed to AWS - enable all the bells and whistles
if os.environ.get(ENV_AWS_MODE):

    import boto3
    from botocore.exceptions import ClientError

    print("Operation mode: deployed to AWS")

    def send_email(email):
        from_address = email["From"].strip()
        to_addresses = (
            [addressee.strip() for addressee in email["To"].split(",")]
            if email["To"]
            else None
        )
        cc_addresses = (
            [addressee.strip() for addressee in email["Cc"].split(",")]
            if email["Cc"]
            else None
        )

        addresses = {"ToAddresses": to_addresses}
        if cc_addresses:
            addresses["CcAddresses"] = cc_addresses

        subject = email["Subject"]
        body = email.get_body()

        response = None
        try:
            # Create a new SES resource and specify a region.
            client = boto3.client("ses", region_name=ENV_AWS_EMAIL_REGION)

            response = client.send_email(
                Destination=addresses,
                Message={
                    "Body": {"Text": {"Charset": "UTF-8", "Data": body}},
                    "Subject": {"Charset": "UTF-8", "Data": subject},
                },
                Source=from_address,
            )
        except ClientError as e:
            print(e.response["Error"]["Message"])
            return False

        return True


# not deployed to AWS - limited functionality
else:  # not os.environ.get(ENV_AWS_MODE)

    print("Operation mode: standalone")

    def send_email(email):
        return False


def get_report_setup(squad, args, project, build_to_report, build_iter):
    """
    Build a dictionary with setup for a single report.

    First - get the previous (time wise) build as 'previous' build
    Then - find the last non-patched build as 'base' build
           if not found - use the last patched build of previous version
    """

    builds = build_iter.collection
    iter = build_iter.clone()

    match_to_build = re.match(REGEXP_STABLE_BUILD, build_to_report.version)
    if not match_to_build:
        print(
            "Build {0} of project {1} did not match the regexp: {2}".format(
                build.version, project.slug, REGEXP_STABLE_BUILD
            )
        )
        return None

    prev_build = build_iter.peek()

    setup = {
        "group": group.slug,
        "project": project.slug,
        "build": build_to_report.version,
        "prev_build": prev_build.version if prev_build else None,
        "base_build": None,
    }

    # should the base build be the same version (with no patches on top)
    same_version = match_to_build.group("count")
    version_to_search_in = None

    # find base build if possible
    for build in build_iter.clone():
        match = re.match(REGEXP_STABLE_BUILD, build.version)

        # if build_to_report is a no-patches-on-top build
        # search for the previous version's build with no patches on top
        # or the last one with patches on top from two versions back

        if same_version:
            # search within the same version
            if (
                match.group("major") == match_to_build.group("major")
                and match.group("minor") == match_to_build.group("minor")
                and match.group("patch") == match_to_build.group("patch")
            ):

                # found a no-patches-on-top version
                if not match.group("count"):
                    setup["base_build"] = build.version
                    return setup

            # previous version - pick the lastest build
            else:
                setup["base_build"] = build.version
                return setup

        # if build_to_report has patches on top
        # search for the current version's build with no patches on top
        # or the last one with patches on top from the previous version

        else:
            # search within the previous version
            if (
                match.group("major") == match_to_build.group("major")
                and match.group("minor") == match_to_build.group("minor")
                and match.group("patch") == match_to_build.group("patch")
            ):
                continue

            if not version_to_search_in:
                version_to_search_in = (
                    match.group("major"),
                    match.group("minor"),
                    match.group("patch"),
                )

            if (
                match.group("major") == version_to_search_in[0]
                and match.group("minor") == version_to_search_in[1]
                and match.group("patch") == version_to_search_in[2]
            ):
                if not match.group("count"):
                    setup["base_build"] = build.version
                    return setup
                else:
                    continue

            setup["base_build"] = build.version
            return setup

    return setup


def generate_reports(squad, args):
    """
    Generate reports as requested in "jobs/*.txt" files.


    TODO: if total number of workers is smaller than the number of jobs,
          each worker does their "column" of jobs (% wise)
    """

    print("Generating reports")

    try:
        worker_index = int(os.environ.get(ENV_PARALLEL_JOB_ID, "1"))
        try:
            worker_count = int(os.environ.get(ENV_PARALLEL_JOB_TOTAL, "1"))
        except ValueError:
            worker_index = 1
            worker_count = 1
            print(
                "Envvar {0} has an invalid value: {1}. Defaulting to single worker.".format(
                    ENV_PARALLEL_JOB_TOTAL, os.environ.get(ENV_PARALLEL_JOB_TOTAL, "1")
                )
            )
    except ValueError:
        worker_index = 1
        worker_count = 1
        print(
            "Envvar {0} has an invalid value: {1}. Defaulting to single worker.".format(
                ENV_PARALLEL_JOB_ID, os.environ.get(ENV_PARALLEL_JOB_ID, "1")
            )
        )

    try:
        os.mkdir(DIRECTORY_REPORTS)
    except FileExistsError:
        pass
    except:
        print(
            "({0}) Exception creating the report directory '{1}': {2}".format(
                job_index, DIRECTORY_REPORTS, sys.exc_info()[0]
            )
        )

    # get the list of job files
    try:
        job_files = os.listdir(DIRECTORY_JOBS)
    except FileNotFoundError as e:
        print(
            "{0}Job directory '{1}' not found".format(
                "({0}) ".format(worker_index) if worker_index != None else "",
                DIRECTORY_JOBS,
            )
        )
        return 1

    if worker_index > len(job_files):
        print(
            "{0}No work for this job (not enough work for all, consider decreasing parallelism.".format(
                "({0}) ".format(worker_index) if worker_index != None else ""
            )
        )
        return 1

    if not job_files:
        print(
            "{0}No reporting jobs found".format(
                "({0}) ".format(worker_index) if worker_index != None else ""
            )
        )
        return 1

    job_files.sort()
    job_index = int(worker_index)

    # run all jobs for the worker
    while job_index <= len(job_files):
        job_file = os.path.join(DIRECTORY_JOBS, job_files[job_index - 1])
        process_job_file(squad, args, job_file, job_index)
        job_index += worker_count

    return 0


def send_reports(squad, args):
    try:
        report_files = os.listdir(DIRECTORY_REPORTS)
    except FileNotFoundError as e:
        print("Report directory '{1}' not found".format(DIRECTORY_REPORTS))
        return 1

    if not report_files:
        print("No summaries found")
        return 1

    report_files.sort()

    for report_file in report_files:
        match = re.match(REGEXP_REPORT_FILE_NAME, report_file)
        if not match:
            print("Skipped a report file with an invalid name: {0}".format(report_file))
            continue

        group = squad.group(match.group("group"))
        if not group:
            print("Unknown group in report file: {0}".format(report_file))
            continue

        project = group.project(match.group("project"))
        if not project:
            print("Unknown project in report file: {0}".format(report_file))
            continue

        build = project.build(match.group("build"))
        if not build:
            print("Unknown build in report file: {0}".format(report_file))
            continue

        with open(os.path.join(DIRECTORY_REPORTS, report_file), "r") as f:
            msg = email.message_from_bytes(bytearray(f.read(), "utf-8"))
            if send_email(msg):
                set_already_processed(project, build)
            else:
                print("Was not able to send email for report: {0}".format(report_file))

    return 0


def send_notifications(squad, args):
    try:
        summary_files = os.listdir(DIRECTORY_SUMMARIES)
    except FileNotFoundError as e:
        print("Summary directory '{1}' not found".format(DIRECTORY_SUMMARIES))
        return 1

    if not summary_files:
        print("No summaries found")
        return 1

    summary_files.sort()

    body = ""
    for summary_file in summary_files:
        with open(os.path.join(DIRECTORY_SUMMARIES, summary_file), "r") as f:
            body += f.read()
    body += "\n-- \nLKFT"

    msg = EmailMessage(policy=policy.default.clone(max_line_length=MAX_URL_LENGTH))
    msg.set_content(body)

    msg["From"] = config.notification_email.get(
        "From", config.default_notification_from
    )

    values = config.notification_email.get("To")
    if len(values):
        msg["To"] = ", ".join(values)
    else:
        msg["To"] = config.default_notification_to

    values = config.notification_email.get("Cc")
    if values and len(values):
        msg["Cc"] = ", ".join(values)
    elif config.default_notification_cc and len(config.default_notification_cc):
        msg["Cc"] = ", ".join(config.default_notification_cc)

    msg["Subject"] = config.notification_email.get(
        "Subject", config.default_notification_subject
    )

    with open("{0}/summary.eml.txt".format(DIRECTORY_SUMMARIES), "w") as f:
        f.write(msg.as_string())

    send_email(email)

    return 0
