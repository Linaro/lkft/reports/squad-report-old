ENV_JOBS = "LKFT_JOBS"

# regexp to parse build's name
REGEXP_STABLE_BUILD = "^v(?P<major>\d+)\.(?P<minor>\d+)(?:\.(?P<patch>\d+))?(?:-(?P<count>\d+)-g(?P<hash>[\da-f]+))?$"

# url of the job details
ENV_JOB_DETAILS_URL = "CI_JOB_URL"

# directory where all the report files are stored
DIRECTORY_REPORTS = "reports"

# parse report file name
REGEXP_REPORT_FILE_NAME = "^(?P<group>.*?)--(?P<project>.*?)--(?P<build>.*)\.eml\..*$"

# current node index (1-based) that is set by gitlab for parallel jobs
ENV_PARALLEL_JOB_ID = "CI_NODE_INDEX"

# total number of parallel jobs that is set by gitlab for parallel jobs
ENV_PARALLEL_JOB_TOTAL = "CI_NODE_TOTAL"

# directory where all the reporting jobs are stored
DIRECTORY_JOBS = "jobs"

# set to any value if running in an AWS environment
ENV_AWS_MODE = "AWS_MODE"

# name of the environment variable that has email region name
ENV_AWS_EMAIL_REGION = "us-east-1"

# directory where all the summary files are stored
DIRECTORY_SUMMARIES = "summaries"

# parse suite url to get suite id
# https://qa-reports.linaro.org/api/environments/74/
REGEXP_ENVIRONMENT_URL = "^.*?://[^/]+/api/environments/(?P<id>\d+).*$"

# parse suite url to get suite id
# https://qa-reports.linaro.org/api/suites/66446/
REGEXP_SUITE_URL = "^.*?://[^/]+/api/suites/(?P<id>\d+).*$"
