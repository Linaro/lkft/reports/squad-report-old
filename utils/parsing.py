import os
import re
import sys
from utils.projects import get_project
from utils.env_vars import ENV_JOBS


def parse_triplet(
    squad, triplet, previous_triplet, require_project=True, require_build=False
):
    """Parse a triplet for the report job definition

    Parameters:
        squad               - Squad API object
        triplet             - a string containing the triplet
        previous_triplet    - a previously parsed triplet or None

    Result:
        if mandatory parts are present, the following dictionary is returned:
            { 'group': <value>, 'project': <value>, 'build': <value }
        otherwise the following dictionary is returned
            { 'errors': [<error message>,...] }
    """
    needs_group = False
    needs_project = False
    needs_build = False
    group_slug = None
    group = None
    project_slug = None
    project = None
    build_version = None
    build = None
    errors = []

    parts = [part.strip() for part in triplet.split(":")]

    if len(parts) == 1:
        if parts[0] != "":
            if not previous_triplet:
                group_slug = parts[0]
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group_slug = None
                    group = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True

            else:  # not first
                build_version = parts[0]
                if not build_version:
                    build_version = None
                    build = None
                    if require_build:
                        errors.append("Missing build")
                    else:
                        needs_build = True
                else:
                    build = previous_triplet["project"].build(build_version)
                    if not build:
                        errors.append("Unknown build '{0}'".format(build_version))
                        build_version = None
                        build = None

    elif len(parts) == 2:
        if not previous_triplet:
            group_slug = parts[0]
            if not group_slug:
                group = None
                group_slug = None
                errors.append("Missing group")
            else:
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group = None
                    group_slug = None

            project_slug = parts[1]
            if not project_slug:
                project_slug = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, previous_triplet["group"], project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

        else:  # not first
            project_slug = parts[0]
            if not project_slug:
                project_slug = None
                project = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, group, project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

            build_version = parts[1]
            if not build_version:
                build_version = None
                build = None
                if require_build:
                    errors.append("Missing build")
                else:
                    needs_build = True
            else:
                build = project.build(build_version)
                if not build:
                    errors.append("Unknown build '{0}'".format(build_version))
                    build_version = None
                    build = None

    else:  # >2
        if not previous_triplet:
            group_slug = parts[0]
            if not group_slug:
                group = None
                group_slug = None
                errors.append("Missing group")
            else:
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group = None
                    group_slug = None

            project_slug = parts[1]
            if not project_slug:
                project_slug = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, group, project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

            build_version = parts[2]
            if not build_version:
                build_version = None
                build = None
                if require_build:
                    errors.append("Missing build")
                else:
                    needs_build = True
            else:
                build = project.build(build_version)
                if not build:
                    errors.append("Unknown build '{0}'".format(build_version))
                    build_version = None
                    build = None

        else:  # not first
            group_slug = parts[0]
            if not group_slug:
                needs_group = True
                group_slug = None
                group = None
            else:
                group = squad.group(group_slug)
                if not group:
                    errors.append("Unknown group '{0}'".format(group_slug))
                    group_slug = None
                    group = None

            project_slug = parts[1]
            if not project_slug:
                project_slug = None
                project = None
                if require_project:
                    errors.append("Missing project")
                else:
                    needs_project = True
            else:
                project = get_project(squad, group, project_slug)
                if not project:
                    errors.append("Unknown project '{0}'".format(project_slug))
                    project_slug = None
                    project = None

            build_version = parts[2]
            if not build_version:
                build_version = None
                build = None
                if require_build:
                    errors.append("Missing build")
                else:
                    needs_build = True
            else:
                build = project.build(build_version)
                if not build:
                    errors.append("Unknown build '{0}'".format(build_version))
                    build_version = None
                    build = None

    result = {}

    if errors:
        result["errors"] = errors
    else:
        if needs_group:
            result["group"] = "?"
        elif group:
            result["group"] = group

        if needs_project:
            result["project"] = "?"
        elif project:
            result["project"] = project

        if needs_build:
            result["build"] = "?"
        elif build:
            result["build"] = build

    return result


def parse_job_definitions(squad, args, allow_incomplete=False):
    jobs = []
    success = True
    sources = (
        ("args", args.jobs),
        (
            ENV_JOBS,
            [item.strip() for item in os.environ.get(ENV_JOBS).split(";")]
            if os.environ.get(ENV_JOBS)
            else None,
        ),
    )

    for source_name, job_defs in sources:
        if not job_defs:
            continue

        if len(job_defs) == 1 and job_defs[0] == "":
            continue

        for job_def in job_defs:
            if not job_def:
                continue

            fq_triplets = []

            triplets = [item.strip() for item in job_def.split(",")]
            if len(triplets) == 1 and triplets[0] == "":
                continue

            previous_triplet = None

            for triplet in triplets:
                parsed_triplet = parse_triplet(
                    squad, triplet, previous_triplet, not allow_incomplete, False
                )

                errors = parsed_triplet.get("errors")
                if errors:
                    success = False
                    print(
                        "Skipping job definition '{0}' due to errors:\n  {1}".format(
                            job_def, "\n  ".join(errors)
                        )
                    )

                else:
                    if (
                        not parsed_triplet.get("group")
                        and previous_triplet
                        and "group" in previous_triplet
                    ):
                        parsed_triplet["group"] = previous_triplet["group"]
                    if (
                        not parsed_triplet.get("project")
                        and previous_triplet
                        and "project" in previous_triplet
                    ):
                        parsed_triplet["project"] = previous_triplet["project"]
                    if not parsed_triplet.get("build"):
                        # TODO: look it up
                        if previous_triplet and "build" in previous_triplet:
                            parsed_triplet["build"] = previous_triplet["build"]

                    fq_triplets.append(parsed_triplet)

                previous_triplet = parsed_triplet

            if fq_triplets:
                job_definition = {"build": fq_triplets[0]}
                if len(fq_triplets) > 1:
                    job_definition["previous"] = fq_triplets[1]
                if len(fq_triplets) > 2:
                    job_definition["base"] = fq_triplets[2]
                jobs.append(job_definition)

        if jobs:
            break  # don't use env var if cmdline arg is provided

    return (success, jobs)


def get_triplet_from_setup(squad, setup):
    group = squad.group(setup["group"])
    if not group:
        return None

    project = group.project(setup["project"])
    if not project:
        return None

    build = project.build(setup["build"])
    if not build:
        return None

    return {"group": group, "project": project, "build": build}


def matches_one_of(string, patterns):
    for _, regexp in patterns:
        if regexp.search(string):
            return True
    return False


def build_patterns(strings):
    result = []

    for string in strings:
        try:
            regexp = re.compile(string)
            result.append((string, regexp))
        except re.error:
            e = sys.exc_info()[1]
            print("Invalid pattern: {0} - {1}".format(string, str(e)))
            return None

    return result
