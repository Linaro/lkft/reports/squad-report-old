# This is a configuration file for LKFT stable report
# Edit this file to adjust the tool to your needs
#

# projects that are mathing this regexp will be scanned for builds
REGEXP_STABLE_PROJECT = "^linux-stable-rc-linux-([\d]+)\.([\d]+)\.y(?:-sanity)?$"


# notification email config
default_notification_from = "LKFT <lkft@linaro.org>"
default_notification_to = "LKFT <lkft@linaro.org>"
default_notification_cc = None
default_notification_subject = "Stable RC reports have been generated"

notification_email = {
    "From": "LKFT <lkft@linaro.org>",
    "To": ["Serge Broslavsky <serge.broslavsky@linaro.org>"],
    "Cc": [],
    "Subject": "Stable RC reports have been generated",
}


# report email config
default_report_from = "LKFT <lkft@linaro.org>"
default_report_to = "LKFT <lkft@linaro.org>"
default_report_cc = None

# project name matching regexp => email setup
report_email_by_project = [
    (
        "^.*$",
        {
            "From": "LKFT <lkft@linaro.org>",
            "To": ["Serge Broslavsky <serge.broslavsky@linaro.org>"],
            "Cc": [],
        },
    ),
]

# build comparison config
squad_data_path = "../squad_data"

regression_config = {
    "days_to_regress": 70,
    "days_to_mention": 70,
}

try:
    from local_settings import *
except ImportError:
    pass
